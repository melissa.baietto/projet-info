from ptd.exportation.abstract_export import AbstractExport
from ptd.model.table import Table
import csv


class ExportCsv(AbstractExport):
    ''' Classe permettant d'exporter une table en fichier csv

    Attributes
    ----------
    fichier : str : nom du fichier que l'on veut créer
    separateur : str : valeur par défaut ";"
    encoding : str : encodage

    Examples
    --------
    >>> table = Table(["var1","var2","var3"],[[None,5,6],[7,None,9],[8,2,1]])
    >>> export = ExportCsv("test.csv","C:/Users/melis/OneDriveGENES/Documents/1A/projet-info/ptd")
    >>> export.appliquer(table)
    '''
    def __init__(self, fichier, dossier, separateur=";", encoding="utf-8"):
        self.fichier=fichier
        self.dossier = dossier
        self.separateur = separateur
        self.encoding = encoding
    
    def appliquer(self, table) : 
        '''Méthode exportant la table en fichier csv

        Parameters
        ----------
        table : Table : table à exporter
        '''
        folder = self.fichier
        dossier = self.dossier

        with open(dossier + "\\" + folder, 'w', encoding=self.encoding) as f : 
            writter = csv.writer(f, delimiter=self.separateur)
            writter.writerow(table.header)
            for i in range(len(table.body)): 
                if table.body[i] == None:
                    writter.writerow('Na')
                else:
                    writter.writerow(table.body[i])
            f.close()

    def __str__(self):
        '''Méthode renvoyant le nom du fichier à exporter
    
        Returns
        -------
        str
        nom du fichier json à importer
    
        Examples
        --------
        >>> export = ExportCsv("test.csv","C:/Users/melis/OneDriveGENES/Documents/1A/projet-info/ptd")
        >>> print(export)
        test.csv
        '''
        return str(self.fichier)


table = Table(["var1","var2","var3"],[[None,5,6],[7,None,9],[8,2,1]])
export = ExportCsv("test.csv","C:/Users/melis/OneDriveGENES/Documents/1A/projet-info/ptd")
export.appliquer(table)

# if __name__=="__main__":
#     import doctest
#     doctest.testmod(verbose = True)