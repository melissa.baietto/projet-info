from abc import ABC, abstractmethod
from ptd.transformation.transformation import Transformation


class AbstractExport(ABC, Transformation): 
    '''Classe abstraite
    '''
    @abstractmethod
    def appliquer(table) : 
        pass