from ptd.transformation.transformation import Transformation
from ptd.model.table import Table


class Selection (Transformation) : 
    '''Classe implémentant une sélection de variables

    Attributes
    ----------
    variables : list

    Examples
    --------

    >>> table_1=Table(["var1", "var2", "var3"], [["a", "b", "c"], ["d", "e", "f"]])
    >>> selection_1 =  Selection(["var1", "var2"])
    >>> selection_1.appliquer(table_1)
    >>> print(table_1)
    ['var1', 'var2']
    [['a', 'b'], ['d', 'e']]
    '''
    def __init__(self, variables):
        self.variables=variables
    
    def appliquer(self, table) :
        '''Méthode appliquant la sélection de variable à la table

        Parameters
        ----------
        table : Table : table sur laquelle on veut sélectionner les variables
        '''
        L1=[]
        L2=[[]*j for j in range(len(table.body)) ]
        for elt in table.header :
            if elt in self.variables :
                i = table.get_indice(elt)
                L1.append(elt)
                for j in range(len(table.body)) :
                    L2[j].append(table.body[j][i])
        table.header=L1
        table.body=L2
    

    def __str__(self):
        '''Méthode renvoyant la liste de variable à sélectionner
    
        Returns
        -------
        liste
        variables à sélectionner
    
        Examples
        --------
        >>> selection_1 =  Selection(["var1", "var2"])
        >>> print(selection_1)
        ['var1', 'var2']
        '''
        return "{}".format(self.variables)


if __name__=="__main__":
    import doctest
    doctest.testmod(verbose = True)
