from ptd.model.table import Table
from ptd.transformation.transformation import Transformation

class MoyenneGlissante:
    '''Classe permettant de faire une moyenne glissante

    Attributes 
    ----------
    variable : str, nom de la variable sur laquelle on veut faire la moyenne glissante
    fenetre : int : période sur laquelle on veut faire la moyenne glissante
    id : str : variable selon laquelle on a trié la table (ex : station ou région)
    id2 : str : deuxième variable surlaquelle on a trié la table (ex : date ou heure)

    Examples
    --------
    >>> table=Table(["var1","var2","var3","var_test","date"],[["a","b","c","1","lundi"],["d","e","f","2","lundi"],["d","e","f","2","mardi"],["d","b","f","2","mardi"],["g","b","i","7","mardi"],["k","e","m","4","lundi"],["n","e","p","8","mardi"],["n","e","p","6","lundi"],["n","e","p","7","mardi"],["n","b","p","8","lundi"],["n","b","p","8","mardi"],["n","b","p","8","lundi"],["n","b","p","8","lundi"],["n","e","p","8","lundi"],["n","e","p","8","mardi"],["n","b","p","8","mardi"]])
    >>> moyenne_glissante1=MoyenneGlissante("var_test",3,"var2","date")
    >>> moyenne_glissante2=MoyenneGlissante("var_test",4,"var2","date")
    >>> moyenne_glissante3=MoyenneGlissante("var_test",7,"var2","date")
    >>> print(moyenne_glissante2.appliquer(table))
    Impossible, choisir une fenetre impaire.
    >>> print(moyenne_glissante3.appliquer(table))
    La fenetre est trop grande : choisir une fenetre plus petite.
    >>> moyenne_glissante1.appliquer(table)
    >>> print(table)
    ['var1', 'var2', 'var3', 'moyenne_glissante', 'date']
    [['a', 'b', 'c', '1', 'lundi'], ['n', 'b', 'p', '5.666666666666667', 'lundi'], ['n', 'b', 'p', '8.0', 'lundi'], ['n', 'b', 'p', '8', 'lundi'], ['d', 'b', 'f', '2', 'mardi'], ['g', 'b', 'i', '5.666666666666667', 'mardi'], ['n', 'b', 'p', '7.666666666666666', 'mardi'], ['n', 'b', 'p', '8', 'mardi'], ['d', 'e', 'f', '2', 'lundi'], ['k', 'e', 'm', '4.0', 'lundi'], ['n', 'e', 'p', '6.0', 'lundi'], ['n', 'e', 'p', '8', 'lundi'], ['d', 'e', 'f', '2', 'mardi'], ['n', 'e', 'p', '5.666666666666667', 'mardi'], ['n', 'e', 'p', '7.666666666666666', 'mardi'], ['n', 'e', 'p', '8', 'mardi']]
    '''

    def __init__(self, variable, fenetre, id, id2=None):
        self.variable = variable
        self.fenetre = fenetre
        self.id=id
        self.id2 = id2
    

    def appliquer(self, table):
        '''Méthode appliquant la moyenne glissante à une table 

        Parameters
        ----------
        table : Table
        '''

        if self.fenetre%2==0:
            return("Impossible, choisir une fenetre impaire.")
        table.trier(self.id,self.id2)
        L=[]
        indice_var=table.get_indice(self.variable)
        indice_id=table.get_indice(self.id)
        indice_id2=table.get_indice(self.id2)
        liste_id=[]
        if self.id2==None:
            for ligne in table.body:
                if ligne[indice_id] not in liste_id:
                    liste_id.append(ligne[indice_id])
            for id in liste_id:
                entree=[]
                premier="0"
                for m in range(len(table.body)):
                    l=table.body[m][indice_id]
                    if l == id:
                        if table.body[m][indice_var]!=None:
                            entree.append(int(table.body[m][indice_var]))
                            if premier=="0":
                                premier=m    
                if len(entree)<self.fenetre:
                    return("La fenetre est trop grande : choisir une fenetre plus petite.")
                sortie = [sum(entree[0:self.fenetre]) / self.fenetre, ]
                for l in range(self.fenetre, len(entree)):
                    sortie.append(sortie[-1] - entree[l - self.fenetre] / self.fenetre + entree[l] / self.fenetre)
                k=0
                for j in range(premier+self.fenetre//2,premier+len(entree)-self.fenetre//2):
                    if table.body[j][indice_var]!=None:
                        table.body[j][indice_var]=str(sortie[k])
                    k+=1
        else:
            liste_id=[]
            for ligne in table.body:
                if [ligne[indice_id],ligne[indice_id2]] not in liste_id:
                    liste_id.append([ligne[indice_id],ligne[indice_id2]])
            for id in liste_id:
                entree=[]
                premier="0"
                for m in range(len(table.body)):
                    l=[table.body[m][indice_id],table.body[m][indice_id2]]
                    if l == id:
                        if table.body[m][indice_var]!=None:
                            entree.append(int(table.body[m][indice_var]))
                            if premier=="0":
                                premier=m    
                if len(entree)<self.fenetre:
                    return("La fenetre est trop grande : choisir une fenetre plus petite.")
                sortie = [sum(entree[0:self.fenetre]) / self.fenetre, ]
                for l in range(self.fenetre, len(entree)):
                    sortie.append(sortie[-1] - entree[l - self.fenetre] / self.fenetre + entree[l] / self.fenetre)
                k=0
                for j in range(premier+self.fenetre//2,premier+len(entree)-self.fenetre//2):
                    if table.body[j][indice_var] != None:
                        table.body[j][indice_var]=str(sortie[k])
                    k+=1
        table.header[indice_var]="moyenne_glissante"


    

if __name__=="__main__":
    import doctest
    doctest.testmod(verbose = True)