from ptd.transformation.transformation import Transformation
from ptd.model.table import Table

class SelectionLigne(Transformation):
    '''Classe implémentant une sélection de lignes

    Attributes
    ----------
    variable : str : variable selon laquelle on veut sélectionner les lignes
    fonction : Fonction : condition de sélection 

    Examples
    --------
    >>> table = Table(["var1","var2","var3"],[['1','5','6'],['7',None,'9'],['8','2','1']])
    >>> def f(x): return int(x)>4
    >>> selection = SelectionLigne("var2",f)
    >>> selection.appliquer(table)
    >>> print(table.body)
    [['1', '5', '6']]
    '''
    def __init__(self, variable, fonction):
        self.variable = variable
        self.fonction = fonction

    def appliquer(self,table):
        '''Méthode appliquant la sélection des lignes à la table
        Parameters
        ----------
        table : Table : table sur laquelle on veut sélectionner les variables

        Examples
        --------
        >>> table2 = Table(["var1","var2","région"],[['1','5','Bretagne'],['7',None,'Normandie'],['8','2','Bretagne'],['6','0','Bretagne']])
        >>> def f2(x): return x == 'Bretagne'
        >>> selection2 = SelectionLigne("région",f2)
        >>> selection2.appliquer(table2)
        >>> print(table2.body)
        [['1', '5', 'Bretagne'], ['8', '2', 'Bretagne'], ['6', '0', 'Bretagne']]
        '''
        L = []
        i = table.get_indice(self.variable)
        for j in range(len(table.body)) :
                if not table.body[j][i]== None:
                    if self.fonction(table.body[j][i])==True:
                        L.append(table.body[j])
        table.body = L
    
    def __str__(self):
        '''Méthode renvoyant la variable selon laquelle on sélectionne les lignes
    
        Returns
        -------
        str
        variables selon laquelle on sélectionne les lignes
    
        Examples
        --------
        >>> def f2(x): return x == 'Bretagne'
        >>> selection = SelectionLigne("région",f2)
        >>> print(selection)
        région
        '''
        return "{}".format(self.variable)



if __name__=="__main__":
    import doctest
    doctest.testmod(verbose = True)