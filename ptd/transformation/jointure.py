from ptd.transformation.transformation import Transformation
from ptd.model.table import Table

class Jointure(Transformation):
    '''Classe permettant de joindre deux tables

    Attributes
    ----------
    variable : str
    cote : str : full_join, gauche ou droite
    table_2 : Table
    Examples
    --------
    >>> jointure1=Jointure("var1","full_join",Table(["var1", "var4", "var5"], [["a", "g", "h"], ["d", "i", "j"],["k","l","m"]]))
    >>> table_1=Table(["var1", "var2", "var3"], [["a", "b", "c"], ["d", "e", "f"], ["o","p","q"]])
    >>> jointure1.appliquer(table_1)
    >>> print(table_1)
    ['var1', 'var2', 'var3', 'var4', 'var5']
    [['a', 'b', 'c', 'g', 'h'], ['d', 'e', 'f', 'i', 'j'], ['o', 'p', 'q', None, None], ['k', None, None, 'l', 'm']]
    '''

    def __init__(self, variable, cote, table_2):
        self.variable=variable
        self.cote=cote
        self.table_2=table_2

    def verifie(self, table1) :
        '''Méthode vérifiant que la variable sur laquelle on joint est bien dans les deux tables
    
        Parameters
        ----------
        table1 : Table

        Retuns
        ------
        bool
        Si le nom de la variable est présent dans les deux tables.
        '''
        if self.variable in table1.header and self.variable in self.table_2.header : 
            return True

    def appliquer(self, table_1) :
        '''Méthode appliquant la jointure
    
        Parameters
        ----------
        table_1
        Table avec laquelle on veut joindre notre table.
        '''
        if not self.verifie(table_1) : 
            return("Impossible de joindre les deux tables")
        if self.cote=="gauche" :
            table1, table2 =table_1, self.table_2
        if self.cote=="droite" :
            table1, table2 =self.table_2, table_1
        if self.cote=="full_join":
            table1, table2 = table_1, self.table_2
        L1=[]
        L3=[]
        for elt in table1.header : 
            L1.append(elt)
        for elt in table2.header :
            if elt not in L1 :
                L1.append(elt)
        L2=table1.body
        i = table1.get_indice(self.variable)
        l = table2.get_indice(self.variable)
        for j in range(len(table2.body)) :
            for k in range(len(table1.body)) :
                if table2.body[j][l]==table1.body[k][i] :
                    for p in range(len(table2.body[j])):
                        if p!=l : 
                            L2[k].append(table2.body[j][p])
                            L3.append(table2.body[j][l])
        for j in range(len(L2)):
                for l in range(len(L1)-len(L2[j])):
                    L2[j].append(None)
        if self.cote=="full_join" : 
            for k in range(len(table2.body)):
                    if table2.body[k][i] not in L3:
                        L4=[]                 
                        for elt in table2.body[k]:
                            L4.append(elt)
                        L2.append(L4)
                        for m in range(len(table1.header)):
                            if table1.header[m] not in table2.header :
                                L2[len(L2)-1].insert(m,None)
        table_1.header=L1
        table_1.body=L2
    
    def __str__(self):
        '''Méthode renvoyant la variable selon laquelle on joint et le côté de la jointure
    
        Returns
        ----------
        str
        variable selon laquelle on joint et côté de la jointure
    
        Examples
        --------
        >>> jointure1=Jointure("var1","droite",Table(["var1", "var4", "var5"], [["a", "g", "h"], ["d", "i", "j"],["k","l","m"]]))
        >>> print(jointure1)
        var1 ; droite
        '''
        return "{} ; {}".format(self.variable,self.cote)


if __name__=="__main__":
    import doctest
    doctest.testmod(verbose = True)


