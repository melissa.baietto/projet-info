from ptd.model.table import Table
from random import randint

def moyenne(variable,table):
    ''' Fonction permettant de calculer la moyenne d'une variable

    Parameters
    -----------
    variable: str, variable sur laquelle on veut calculer la moyenne
    table : Table

    Return
    -------
    moyenne : int

    Examples
    --------
    >>> table_test=Table(["var1","var2","var3"],[["1","2","3"],["4","5", None],[None,None,"7"]])
    >>> print(moyenne("var3",table_test))
    5.0
    '''
    i = table.get_indice(variable)
    m=0
    compteur=0
    for ligne in table.body :
        if ligne[i]!=None :
            m+=float(ligne[i])
            compteur+=1  
    return(m/compteur) 

def variance(variable,table):
    ''' Fonction permettant de calculer la variance d'une variable

    Parameters
    -----------
    variable: str, variable sur laquelle on veut calculer la variance
    table : Table

    Return
    -------
    variance : int

    Examples
    --------
    >>> table_test=Table(["var1","var2","var3"],[["1","2","3"],["4","5", None],[None,None,"7"]])
    >>> print(variance("var3",table_test))
    4.0
    '''
    i = table.get_indice(variable)
    v=0
    compteur=0
    s=0
    m=moyenne(variable, table)
    for ligne in table.body :
        if ligne[i]!=None :
            s+=(float(ligne[i])-m)**2
            compteur+=1
    return(s/compteur)


def quicksort(list):
    # Selection d'un élément pivot au hasard
    pivot_index = randint(0, len(list))
    pivot=list[pivot_index]
    # Creation des listes nécessaires
    inf_pivot = []
    sup_pivot = []
    eq_pivot = [pivot]
    # Parcours des éléments pour les mettre à droite ou gauche du pivot
    for element in list :
        if element > pivot : inf_pivot.append(element)
        elif element < pivot : sup_pivot.append(element)
        else: eq_pivot.append(element)
    # Trie des listes de manière récursive
    inf_pivot_sorted = quicksort(inf_pivot)
    sup_pivot_sorted = quicksort(sup_pivot)
    return inf_pivot_sorted + eq_pivot + sup_pivot


if __name__=="__main__":
    import doctest
    doctest.testmod(verbose = True)