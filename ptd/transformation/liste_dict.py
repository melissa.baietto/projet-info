class ListeDictionnaire:
    '''Classe implémentant des objets possédant un attribut de type list(dict) 

    Attributes
    ----------
    l: list(dict) : une liste de dictionnaire

    Examples
    --------
    >>> liste=ListeDictionnaire([{"cle_1" : 'a',"cle_2" : 'b', "cle_3": {"cle31": "h", "cle32" : "i", "cletest":{"cletest1":"lol","cletest2":"lolilol"}}},{"cle_1" : 'c',"cle_2" : 'd', "cle_3": {"cle31": "h", "cle32" : "i"}}])
    >>> liste1=liste.transforme()
    >>> print(liste1)
    [{'cle_1': 'a', 'cle_2': 'b', 'cle31': 'h', 'cle32': 'i', 'cletest': {'cletest1': 'lol', 'cletest2': 'lolilol'}}, {'cle_1': 'c', 'cle_2': 'd', 'cle31': 'h', 'cle32': 'i'}]
    >>> liste2=liste1.transforme()
    >>> print(liste2)
    [{'cle_1': 'a', 'cle_2': 'b', 'cle31': 'h', 'cle32': 'i', 'cletest1': 'lol', 'cletest2': 'lolilol'}, {'cle_1': 'c', 'cle_2': 'd', 'cle31': 'h', 'cle32': 'i'}]
    '''
    def __init__(self,l):
        self.l=l

    def type_cle(self):
        '''Méthode vérifiant si chaque valeur des dictionnaire constituant la liste de dictionnaires n'est pas elle-même un dictionnaire
    
        Returns
        -------
        bool
        True : Si au moins une valeur est un dictionnaire

        '''
        for dictionnaire in self.l: 
            for cle in dictionnaire : 
                if type(dictionnaire[cle])==dict:
                    return False
        return True
    
    def transforme(self):
        '''Méthode permettant d'applatir la liste de dictionnaires pour les valeurs étant des dictionnaires

        Returns
        -------
        Nouvelle liste de dictionnaire applatie : ListeDictionnaire

        '''
        L=[]
        for dictionnaire in self.l :
            dict1={}
            for cle in dictionnaire : 
                if type(dictionnaire[cle])!=dict:
                    dict1[cle]=dictionnaire[cle]
                else:
                    for cle2 in dictionnaire[cle].keys():
                        dict1[cle2]=dictionnaire[cle][cle2]
            L.append(dict1)
        return(ListeDictionnaire(L))

        

    def __str__(self):
        '''Méthode renvoyant la liste de dictionnaire

        Returns
        -------
        list(Dict) : la liste de dictionnaire

        '''
        return("{}".format(self.l))

if __name__=="__main__":
    import doctest
    doctest.testmod(verbose = True)



