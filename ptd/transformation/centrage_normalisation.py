from abc import ABC, abstractmethod
from math import sqrt
from ptd.transformation.transformation import Transformation
import ptd.transformation.fonctions as f
from ptd.model.table import Table

class CentrageNormalisation(ABC, Transformation):
    '''Classe centrant et réduisant les données

    Attributes
    ----------
    variables : list : liste de variables qu'on veut centrer et éventuellement réduire
    reduire : bool : option permettant de décider de réduire les données ou non, vaut True initialement

    Examples
    --------
    >>> table_test=Table(["var1","var2","var3"],[["6","4","3"],["46","8", "14"],[None,None,"7"]])
    >>> centrage1=CentrageNormalisation(["var1","var2","var3"],False)
    >>> centrage1.appliquer(table_test)
    >>> print(table_test.body)
    [['-20.0', '-2.0', '-5.0'], ['20.0', '2.0', '6.0'], [None, None, '-1.0']]
       '''
    def __init__(self, variables, reduire=True):
        self.variables=variables
        self.reduire = reduire

    def appliquer(self, table):
        '''Méthode appliquant la normalisation à la table

        Parameters
        ----------
        table : Table : table sur laquelle on applique le centrage

        Examples 
        --------
        >>> table_test=Table(["var1","var2","var3"],[["6","4","3"],["46","8", "14"],[None,None,"7"]])
        >>> centrage2=CentrageNormalisation(["var1","var2","var3"])
        >>> centrage2.appliquer(table_test)
        >>> print(table_test.body)
        [['-1.0', '-1.0', '-1.0998533626601497'], ['1.0', '1.0', '1.3198240351921797'], [None, None, '-0.21997067253202995']]
        '''
        for variable in self.variables :
            i=table.get_indice(variable)
            m=f.moyenne(variable, table)
            v=f.variance(variable, table)
            for ligne in table.body:
                if ligne[i]!=None:
                    if self.reduire==True:
                        ligne[i]=str((float(ligne[i])-m)/sqrt(v))
                    else : 
                        ligne[i]=str(float(ligne[i])-m)

    def __str__(self):
        '''Méthode renvoyant le nom des variables à centrer et éventuellement réduire
    
        Returns
        -------
        list
        liste des variables
    
        Examples
        --------
        >>> centrage2=CentrageNormalisation(["var1","var2","var3"])
        >>> print(centrage2)
        ['var1', 'var2', 'var3']
        '''
        return("{}".format(self.variables))


if __name__=="__main__":
    import doctest
    doctest.testmod(verbose = True)

            



