from ptd.transformation.transformation import Transformation
from ptd.model.table import Table
from datetime import datetime
from ptd.importation.import_json import ImportJson


class Fenetrage(Transformation):
    '''Classe permettant de choisir une periode temporelle 

    Attributes
    ----------
    debut : str : date de debut de la periode 
    fin : str : date de fin de la periode
    var : str : nom de la variable de temps 

    Examples
    --------
    >>> importer_ex = ImportJson("2022-01.json.gz","C:/Users/melis/OneDriveGENES/Documents/1A/projet-info/ptd","utf-8")
    >>> table_electricite = importer_ex.appliquer()
    >>> fenetrage_1=Fenetrage('2022-01-20T00:00:00+01:00','2022-01-21T00:00:00+01:00','date_heure')
    >>> fenetrage_1.appliquer(table_electricite)
    >>> for i in range(5): print(table_electricite.body[i])
    ['consommation-quotidienne-brute-regionale', '4fbd5a90b1e3728bf491c89da8763b02449321cb', 12409, 10038, '11', '2022-01-20', 'Consolidé', 22447, '2022-01-20T00:00:00+01:00', 'Île-de-France', 12409, 'Définitif', '00:00', '2022-03-24T13:34:22.845+01:00', None, None]
    ['consommation-quotidienne-brute-regionale', '82039e40ed3b7a476d1dd1e2a21e413d47f9699a', 5492, 3975, '28', '2022-01-20', 'Consolidé', 9467, '2022-01-20T00:00:00+01:00', 'Normandie', 5492, 'Définitif', '00:00', '2022-03-24T13:34:22.845+01:00', None, None]
    ['consommation-quotidienne-brute-regionale', 'a33ccb1dea2a1a7105d1884138f6852a6263b1b4', 2703, 6642, '75', '2022-01-20', 'Consolidé', 11770, '2022-01-20T00:00:00+01:00', 'Nouvelle-Aquitaine', 5128, 'Définitif', '00:00', '2022-03-24T13:34:22.845+01:00', 'Définitif', 2425]
    ['consommation-quotidienne-brute-regionale', '9a59001df790daada84f04368f72c07b8b342f56', 5334, 6198, '93', '2022-01-20', 'Consolidé', 11532, '2022-01-20T00:00:00+01:00', "Provence-Alpes-Côte d'Azur", 5334, 'Définitif', '00:00', '2022-03-24T13:34:22.845+01:00', None, None]
    ['consommation-quotidienne-brute-regionale', 'a498d9399656f7fedc9b79d66390ffa55bfbce3f', None, 3440, '53', '2022-01-20', 'Consolidé', None, '2022-01-20T00:30:00+01:00', 'Bretagne', None, None, '00:30', '2022-03-24T13:34:22.845+01:00', None, None]
    '''
    def __init__(self, debut, fin, var):
        self.debut=debut
        self.fin=fin
        self.var=var

    def appliquer(self, table):
        '''Méthode appliquant le fenêtrage à la table

        Parameters
        ----------
        table : Table : table sur laquelle on applique le fenêtrage
        '''
        L=[]
        i = table.get_indice(self.var)
        for elt in table.body :
            date=datetime.strptime(elt[i], "%Y-%m-%dT%H:%M:%S+01:00")
            deb=datetime.strptime(self.debut, "%Y-%m-%dT%H:%M:%S+01:00")
            fin=datetime.strptime(self.fin, "%Y-%m-%dT%H:%M:%S+01:00")
            if deb<=date<=fin:
                L.append(elt)
        table.body=L

    def __str__(self):
        '''Méthode renvoyant la période à fenêtrer
    
        Returns
        -------
        str
        début de la période et fin de la période
    
        Examples
        --------
        >>> fenetrage_1=Fenetrage('2013-01-20T00:00:00+01:00','2013-01-21T00:00:00+01:00','date_heure')
        >>> print(fenetrage_1)
        2013-01-20T00:00:00+01:00 ; 2013-01-21T00:00:00+01:00
        '''
        return "{} ; {}".format(self.debut,self.fin)
        

if __name__=="__main__":
    import doctest
    doctest.testmod(verbose = True)




