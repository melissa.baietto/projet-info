from ptd.transformation.transformation import Transformation
from ptd.model.table import Table
import ptd.transformation.fonctions as f
from ptd.model.table import Table

class Agregation(Transformation):
    '''Classe permettant d'agréger les lignes

    Attributes
    ----------
    variable : str, premiere variable selon laquelle on veut agréger (ex: station, région,...)
    variable2 : str, seconde variable selon laquelle on veut agréger (ex: date), valeur par défaut : None
    fonction : str, peut être moyenne ou somme, selon la fonction qu'on veut utiliser pour traiter les
    autres variables que variable et variable2, valeur par défaut: None

    Examples
    --------
    >>> table=Table(["var1","var2","var3","region","date"],[["1","2","3","Bretagne","lundi"],["10","7","2","Auvergne","lundi"],["4","5","6","Auvergne","mardi"],["6","7","8","Bretagne","mardi"],["6","7","8","Bretagne","lundi"],["4","5","6","Normandie","jeudi"]])
    >>> agregation=Agregation("region","date","somme")
    >>> agregation.appliquer(table)
    >>> print(table)
    ['region', 'date', 'var1', 'var2', 'var3']
    [['Bretagne', 'lundi', '7.0', '9.0', '11.0'], ['Bretagne', 'mardi', '6.0', '7.0', '8.0'], ['Auvergne', 'lundi', '10.0', '7.0', '2.0'], ['Auvergne', 'mardi', '4.0', '5.0', '6.0'], ['Normandie', 'jeudi', '4.0', '5.0', '6.0']]

    '''
    def __init__(self,variable,variable2=None,fonction="moyenne"):
        self.variable = variable
        self.variable2 = variable2
        self.fonction=fonction
    
    def appliquer(self,table):
        '''Méthode aggrégant la table selon la ou les variables données

        Parameters
        ----------
        table : Table : table sur laquelle on applique l'aggrégation'
        '''
        if self.fonction !="moyenne" and self.fonction != "somme":
            return("Impossible, veuillez choisir si vous voulez faire la somme ou la moyenne")
        L=[]
        for ligne in table.body:
            dict1={}
            for var in table.header: 
                j = table.get_indice(var)
                dict1[var]=ligne[j]
            L.append(dict1)
        nom=[]
        i=table.get_indice(self.variable)
        for ligne in table.body : 
            if ligne[i] not in nom : 
                nom.append(ligne[i])
        dict2={}
        for elt in nom :
            L1=[]
            for k in range(len(table.body)):
                if table.body[k][i]==elt:
                    L1.append(L[k])
            dict2[elt]=L1
        if self.variable2!=None:
            for cle in dict2.keys():
                nom=[]
                for dictionnaire in dict2[cle] : 
                    if dictionnaire[self.variable2] not in nom : 
                        nom.append(dictionnaire[self.variable2])
                dict3={}
                for elt in nom :
                    L1=[]
                    for dictionnaire in dict2[cle]:
                        if dictionnaire[self.variable2]==elt:
                            L1.append(dictionnaire)
                        dict3[elt]=L1
                dict2[cle]=dict3
            nb_modalite=0
            for dictionnaire in dict2.values():
                for cle in dictionnaire.keys():
                    nb_modalite+=1
            L_body=[[]*k for k in range(nb_modalite)]

            L_header=[self.variable,self.variable2]
            j=0
            for dictionnaire1 in dict2.values():
                for liste in dictionnaire1.values():
                    L_body[j].append(liste[0][self.variable])
                    L_body[j].append(liste[0][self.variable2])
                    for cle in liste[0].keys():
                        if cle!=self.variable and cle!=self.variable2:
                            if cle not in L_header:
                                L_header.append(cle)
                            m=0
                            v=0
                            for dictionnaire2 in liste : 
                                if dictionnaire2[cle] == None: 
                                    v=v+1
                                else:
                                    m+=float(dictionnaire2[cle])
                            if self.fonction=="moyenne":
                                if len(liste)-v > 0 :
                                    L_body[j].append(str(m/(len(liste)-v)))
                                else:
                                    L_body[j].append(None)
                            if self.fonction=="somme":
                                L_body[j].append(str(m))
                    j+=1
            
            table.header=L_header
            table.body=L_body
        else : 
            nb_modalite=0
            for cle in dict2.keys():
                nb_modalite+=1
            L_body=[[]*k for k in range(nb_modalite)]
            j=0
            L_header=[self.variable]
            for liste in dict2.values():
                cle = liste[0][self.variable]
                L_body[j].append(cle)
                for cle2 in liste[0].keys():
                    if cle2 not in L_header : 
                        L_header.append(cle2)
                    if cle2!=self.variable:
                        m=0
                        v=0
                        for dictionnaire in liste :
                            if dictionnaire[cle2] == None: 
                                v=v+1
                            else:
                                m+=float(dictionnaire[cle2])
                        if self.fonction=="moyenne":
                            if len(liste)-v > 0 :
                                L_body[j].append(str(m/(len(liste)-v)))
                            else:
                                L_body[j].append(None)
                        if self.fonction=="somme":
                            L_body[j].append(str(m))
                j+=1
            table.header=L_header
            table.body=L_body
                
        

if __name__=="__main__":
    import doctest
    doctest.testmod(verbose = True)


