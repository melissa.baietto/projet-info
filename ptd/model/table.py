class Table : 
    ''' Classe implémentant une table composée d'un header et d'un body

    Attributes
    ----------
    header : list(str)
    body : list(list(str))

    Examples
    --------
    >>> tab = Table(['var1', 'var2', 'var3','var4','var5'],[['2','3','4','9','3'], ['4','7','8','5','0']])
    >>> print(tab.header)
    ['var1', 'var2', 'var3', 'var4', 'var5']
    >>> print(tab.body)
    [['2', '3', '4', '9', '3'], ['4', '7', '8', '5', '0']]
    '''
    def __init__(self, header, body): 
        self.header=header
        self.body=body

    def get_indice(self, variable):

        '''Méthode donnant l'indice de la variable associé au nom de variable demandé
    
        Parameters
        ----------
        variable : str : nom de la variable dont on veut le numéro d'indice
    
        Returns
        -------
        int
        numéro de l'indice de la variable
    
        Examples
        --------
        >>> tab = Table(['var1', 'var2', 'var3'],[['2','3','4'], ['4','7','8']])
        >>> tab.get_indice('var2')
        1
        '''
        for i in range(len(self.header)):
            if self.header[i]==variable:
                return(i)
    

    def rename_var(self,new_name,former_name):
        '''Méthode renommant une variable choisie
    
        Parameters
        ----------
        new-name : str : nouveau nom de la variable
        former_name : str : variable qu'on veut renommer
    
        Examples
        --------
        >>> tab = Table(['var1', 'var2', 'var3'],[['2','3','4'], ['4','7','8']])
        >>> tab.rename_var('variable1','var1')
        >>> print(tab.header)
        ['variable1', 'var2', 'var3']
        '''
        i=self.get_indice(former_name)
        self.header[i]= new_name


    def __str__(self):
        '''Méthode renvoyant la liste des variables et le tableau du jeu de données
    
        Returns
        ----------
        str
        liste de variable
        liste de liste des données
    
        Examples
        --------
        >>> tab = Table(['var1', 'var2', 'var3'],[['2','3','4'], ['4','7','8']])
        >>> print(tab)
        ['var1', 'var2', 'var3']
        [['2', '3', '4'], ['4', '7', '8']]
        '''
        return("{}\n{}".format(self.header, self.body))

    def trier(self,id1,id2):
        '''Méthode permettant de trier la table selon une ou deux variables

        Parameters
        ----------
        id1 : premiere variable selon laquelle on veut trier
        id2 : seconde variable selon laquelle on veut trier

        Examples
        --------
        >>> table=Table(["var1","var2","var3","var_test","date"],[["a","b","c","1","lundi"],["d","e","f","2","lundi"],["d","e","f","2","mardi"],["d","b","f","2","mardi"],["g","b","i","7","mardi"],["k","e","m","4","lundi"],["n","e","p","8","mardi"],["n","e","p","6","lundi"],["n","e","p","7","mardi"],["n","b","p","8","lundi"],["n","b","p","8","mardi"],["n","b","p","8","lundi"],["n","b","p","8","lundi"],["n","e","p","8","lundi"],["n","e","p","8","mardi"],["n","b","p","8","mardi"]])
        >>> table.trier("var2","date")
        >>> print(table)
        ['var1', 'var2', 'var3', 'var_test', 'date']
        [['a', 'b', 'c', '1', 'lundi'], ['n', 'b', 'p', '8', 'lundi'], ['n', 'b', 'p', '8', 'lundi'], ['n', 'b', 'p', '8', 'lundi'], ['d', 'b', 'f', '2', 'mardi'], ['g', 'b', 'i', '7', 'mardi'], ['n', 'b', 'p', '8', 'mardi'], ['n', 'b', 'p', '8', 'mardi'], ['d', 'e', 'f', '2', 'lundi'], ['k', 'e', 'm', '4', 'lundi'], ['n', 'e', 'p', '6', 'lundi'], ['n', 'e', 'p', '8', 'lundi'], ['d', 'e', 'f', '2', 'mardi'], ['n', 'e', 'p', '8', 'mardi'], ['n', 'e', 'p', '7', 'mardi'], ['n', 'e', 'p', '8', 'mardi']]
        '''
        L2=[]
        i=self.get_indice(id1)
        m=self.get_indice(id2)
        for ligne in self.body:
            if ligne[i]!=None and ligne[m]!=None:
                L2.append(ligne)
        self.body=L2
        L=[]
        for ligne in self.body:
            dict1={}
            for var in self.header: 
                j = self.get_indice(var)
                dict1[var]=ligne[j]
            L.append(dict1)
        nom=[]
        for ligne in self.body : 
            if ligne[i] not in nom : 
                nom.append(ligne[i])
        dict2={}
        for elt in nom :
            L1=[]
            for k in range(len(self.body)):
                if self.body[k][i]==elt:
                    L1.append(L[k])
            dict2[elt]=L1
        if id2!=None:
            for cle in dict2.keys():
                nom=[]
                for dictionnaire in dict2[cle] : 
                    if dictionnaire[id2] not in nom : 
                        nom.append(dictionnaire[id2])
                dict3={}
                for elt in nom :
                    L1=[]
                    for dictionnaire in dict2[cle]:
                        if dictionnaire[id2]==elt:
                            L1.append(dictionnaire)
                        dict3[elt]=L1
                dict2[cle]=dict3
            L2=[[]*l for l in range(len(self.body))]
            j=0
            for dictionnaire1 in dict2.values():
                for liste in dictionnaire1.values():
                    for dictionnaire2 in liste:
                        for valeur in dictionnaire2.values():
                            L2[j].append(valeur)
                        j+=1
        else:
            L2=[[]*l for l in range(len(self.body))]
            j=0
            for liste in dict2.values():
                for dictionnaire in liste:
                    for valeur in dictionnaire.values():
                        L2[j].append(valeur)
                    j+=1
        self.body=L2

    def trier_num(self,variable):
        '''Methode permettant de trier la table selon une variable numérique

        Parameters
        ----------
        variable: str, la vraiable selon laquelle on veut trier la table

        Examples
        ---------
        >>> tab = Table(['var1', 'var2', 'var3'],[['2','9','4'], ['4','7','8']])
        >>> tab.trier_num("var2")
        >>> print(tab)
        ['var1', 'var2', 'var3']
        [['4', '7', '8'], ['2', '9', '4']]
        '''
        L2 = self.body
        i = self.get_indice(variable)
        for j in range(len(L2)):
            for k in range(0,len(L2)-j-1):
                if int(L2[k+1][i])<int(L2[k][i]):
                    L2[k+1],L2[k] = L2[k], L2[k+1]
        self.body=L2


# table=Table(["var1","var2","var3","var_test","date"],[["a","b","c","1","lundi"],["d","e","f","2","mardi"],["d","e","f","2","mardi"],["d","b","f","2","mardi"],["g","b","i","7","mardi"],["k","e","m","4","lundi"],["n","e","p","8","mardi"],["n","e","p","6","lundi"],["n","e","p","7","mardi"],["n","b","p","8","lundi"]])
# table.trier("var2","date")
# print(table)




if __name__=="__main__":
    import doctest
    doctest.testmod(verbose = True)




