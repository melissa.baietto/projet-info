from ptd.transformation.transformation import Transformation
from ptd.model.table import Table
from ptd.transformation.selection import Selection
from ptd.transformation.jointure import Jointure
from ptd.importation.import_json import ImportJson
from ptd.transformation.fenetrage import Fenetrage
from ptd.transformation.agregation import Agregation
from ptd.transformation.centrage_normalisation import CentrageNormalisation
import ptd.transformation.fonctions as f
from ptd.transformation.selection_lignes import SelectionLigne
from ptd.exportation.export_csv import ExportCsv
from ptd.transformation.moyenne_glissante import MoyenneGlissante

class Pipeline : 
    ''' Classe implémentant une liste de transformations

    Attributes
    ----------
    transformation : list(Transformation)

    Examples
    --------
    >>> table = Table(["var1", "var4", "var5"], [["a", "g", "h"], ["d", "i", "j"],["k","l","m"]])
    >>> pipeline_1= Pipeline()
    >>> S = Selection(["var1"])
    >>> J = Jointure("var1","full_join",table)
    >>> pipeline_1.ajoute_transformations(S)
    >>> pipeline_1.ajoute_transformations(J)
    >>> table_1=Table(["var1", "var2", "var3"], [["a", "b", "c"], ["d", "e", "f"]])
    >>> pipeline_1.run(table_1)
    >>> print(table_1)
    ['var1', 'var4', 'var5']
    [['a', 'g', 'h'], ['d', 'i', 'j'], ['k', 'l', 'm']]
    '''
    def __init__(self): 
        self.transformation=[]

    def ajoute_transformations(self, transformation): 
        '''Méthode ajoutant une transformation dans la liste des transformations
    
        Parameters
        ----------
        transformation : Transformation : une transformation à ajouter dans le pipeline
    
        Examples
        --------
        >>> pipeline = Pipeline()
        >>> S = Selection("var1")
        >>> pipeline.ajoute_transformations(Selection("var1"))
        '''
        self.transformation.append(transformation)

    def run(self, table): 
        '''Méthode effectuant les transformations de la liste sur la table
    
        Parameters
        ----------
        table : Table : table sur laquelle on effectue les transformations
        '''
        for t in self.transformation : 
            t.appliquer(table)

    def __str__(self) :
        '''Méthode renvoyant la liste de transformations
    
        Returns
        ----------
        list
        liste des Transformations
        '''
        return "{}".format(self.transformation)


if __name__=="__main__":
    import doctest
    doctest.testmod(verbose = True)

