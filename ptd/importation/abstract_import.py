from abc import ABC, abstractmethod
from ptd.transformation.transformation import Transformation


class AbstractImport(ABC, Transformation):
    ''' Classe abstraite
    '''
    @abstractmethod
    def appliquer(self,fichier):
        pass