from ptd.importation.abstract_import import AbstractImport
import gzip
import csv
from ptd.model.table import Table

class ImportCsv(AbstractImport):
    ''' Classe permettant d'importer un fichier csv

    Attributes
    ----------
    fichier : str : nom du fichier que l'on veut créer
    dossier : str : chemin du dossier où se situe le fichier
    separateur : str : valeur par défaut ";"
    encoding : str : encodage

    Examples 
    --------
    >>> imp_csv = ImportCsv("synop.202201.csv.gz","C:/Users/melis/OneDriveGENES/Documents/1A/projet-info/ptd","utf8")
    >>> table_meteo = imp_csv.appliquer()
    >>> print(table_meteo.body[0])
    ['07005', '20220101000000', '102420', '-10', '7', '170', '2.100000', '284.450000', '283.350000', '93', '20000', '0', None, None, None, None, None, None, None, None, '101540', None, None, '430', None, None, None, None, '282.050000', None, None, '4.500000', '5.500000', '-10', '1', '0.000000', None, None, '0.000000', '0.000000', '0.000000', '-0.100000', '-0.100000', None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, '']
    '''
    def __init__(self, fichier, dossier, encodage, separateur=";"):
        self.fichier = fichier
        self.dossier = dossier
        self.encodage = encodage
        self.separateur = separateur

    def appliquer(self):
        '''Méthode appliquant l'importation du fichier csv en type Table
    
        Returns
        -------
        Table 
        le fichier csv de type Table avec un header et un body
    
        Examples
        --------
        >>> imp_csv2 = ImportCsv("postesSynopAvecRegions.csv","C:/Users/melis/OneDriveGENES/Documents/1A/projet-info/ptd","utf8")
        >>> table_station_region = imp_csv2.appliquer()
        >>> print(table_station_region.header)
        ['ID', 'Nom', 'Latitude', 'Longitude', 'Altitude', 'Region']
        '''
        
        folder = self.dossier
        filename = self.fichier
        data = []
        if filename[-1] =='z' and filename[-2]=='g':
            with gzip.open(folder+"\\"+filename, mode='rt',encoding=self.encodage) as gzfile :
                synopreader = csv.reader(gzfile, delimiter=self.separateur)
                for row in synopreader :
                    data.append(row)
        else:
            with open(folder+"\\"+filename, newline='') as csvfile:
                synopreader = csv.reader(csvfile, delimiter=self.separateur)
                for row in synopreader :
                    data.append(row)
        L1=data[0]
        L2=[]
        for i in range(1,len(data)):
            L2.append(data[i])
        for j in range(len(L2)):
            for k in range(len(L2[0])):
                if L2[j][k]=='mq':
                    L2[j][k]=None
        return(Table(L1,L2))

    def __str__(self):
        '''Méthode renvoyant le nom du fichier csv à importer 
    
        Returns
        -------
        str
        nom du fichier csv à importer
    
        Examples
        --------
        >>> imp_csv = ImportCsv("synop.202201.csv.gz","C:/Users/melis/OneDriveGENES/Documents/1A/projet-info/ptd","utf8")
        >>> print(imp_csv)
        synop.202201.csv.gz
        '''
        return str(self.fichier)



if __name__=="__main__":
    import doctest
    doctest.testmod(verbose = True)



