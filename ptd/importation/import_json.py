from ptd.importation.abstract_import import AbstractImport
import gzip
import json
from ptd.model.table import Table
from ptd.transformation.liste_dict import ListeDictionnaire


class ImportJson(AbstractImport):
    '''Classe permettant d'importer un fichier de type json

    Attributes
    ----------
    __fichier : str
    __dossier : str
    __encodage : str

    Examples
    --------
    >>> importer_ex = ImportJson("2022-01.json.gz","C:/Users/melis/OneDriveGENES/Documents/1A/projet-info/ptd","utf-8")
    >>> table_electricite = importer_ex.appliquer()
    >>> print(table_electricite.header)
    ['datasetid', 'recordid', 'consommation_brute_gaz_grtgaz', 'consommation_brute_electricite_rte', 'code_insee_region', 'date', 'statut_rte', 'consommation_brute_totale', 'date_heure', 'region', 'consommation_brute_gaz_totale', 'statut_grtgaz', 'heure', 'record_timestamp', 'statut_terega', 'consommation_brute_gaz_terega']
    '''

    def __init__(self,fichier,dossier,encodage):
        self.__fichier = fichier
        self.__dossier = dossier
        self.__encodage = encodage



    def appliquer(self):
        '''Méthode appliquant l'importation du fichier json en type Table
    
        Returns
        -------
        Table 
        le fichier json de type Table avec un header et un body
        '''
        L1=[]
        L2=[]
        folder = self.__dossier
        filename = self.__fichier
        data=None
        with gzip.open(folder  +"\\"+filename, mode='rt',encoding=self.__encodage) as gzfile :
            data = json.load(gzfile)
            liste1=ListeDictionnaire(data)
        while not liste1.type_cle():
            liste1=liste1.transforme()
        L1=[]
        L2=[[]*i for i in range(len(liste1.l))]
        for cle in liste1.l[0].keys():
            L1.append(cle)
            L2[0].append(liste1.l[0][cle])
        for i in range(1,len(liste1.l)):
            for cle1 in L1:
                if cle1 in liste1.l[i].keys():
                    L2[i].append(liste1.l[i][cle1])
                else : 
                    L2[i].append(None)
        for i in range(1,len(liste1.l)):   
            for cle2 in liste1.l[i].keys():
                if cle2 not in L1:
                    L1.append(cle2)
                    L2[i].append(liste1.l[i][cle2])
                    for j in range(len(liste1.l)):
                        if j!=i:
                            if cle2 in liste1.l[j].keys():
                                L2[j].append(liste1.l[j][cle2])
                            else :
                                L2[j].append(None)
        return(Table(L1,L2))

    def __str__(self):
        '''Méthode renvoyant le nom du fichier json à importer 
    
        Returns
        -------
        str
        nom du fichier json à importer
    
        Examples
        --------
        >>> imp_js = ImportJson("2022-01.json.gz","C:/Users/melis/OneDriveGENES/Documents/1A/projet-info/ptd","utf-8")
        >>> print(imp_js)
        2022-01.json.gz
        '''
        return str(self.__fichier)


if __name__=="__main__":
    import doctest
    doctest.testmod(verbose = True)


