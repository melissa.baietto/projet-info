from ptd.transformation.transformation import Transformation
from ptd.model.table import Table
from ptd.transformation.selection import Selection
from ptd.transformation.jointure import Jointure
from ptd.importation.import_json import ImportJson
from ptd.importation.import_csv import ImportCsv
from ptd.transformation.fenetrage import Fenetrage
from ptd.transformation.agregation import Agregation
from ptd.transformation.centrage_normalisation import CentrageNormalisation
import ptd.transformation.fonctions as f
from ptd.transformation.selection_lignes import SelectionLigne
from ptd.exportation.export_csv import ExportCsv
from ptd.transformation.moyenne_glissante import MoyenneGlissante
from ptd.model.pipeline import Pipeline
import re

# #Exemple d'utilisation du programme

# #Importation en format Table des fichiers météo, énergétiques et stations avec régions
# imp_csv2 = ImportCsv("postesSynopAvecRegions.csv","C:/Users/melis/OneDriveGENES/Documents/1A/projet-info/ptd","ISO-8859-1")
# table_station_region = imp_csv2.appliquer()

# imp_csv = ImportCsv("synop.202201.csv.gz","C:/Users/melis/OneDriveGENES/Documents/1A/projet-info/ptd","ISO-8859-1")
# table_meteo = imp_csv.appliquer()
# importer_ex = ImportJson("2022-01.json.gz","C:/Users/melis/OneDriveGENES/Documents/1A/projet-info/ptd","ISO-8859-1")
# table_electricite = importer_ex.appliquer()

# #Création d'une table avec la table météo et celle avec les régions

# #On renomme la variable 'numer_sta' en 'ID' pour pouvoir joindre les deux tables
# table_meteo.rename_var('ID','numer_sta')

# #On joint les deux tables en full join
# jointure = Jointure('ID','full_join',table_station_region)
# jointure.appliquer(table_meteo)

# #On aggrège par la moyenne selon les régions et la date pour pouvoir joindre cette table à celle d'électricité
# #On supprime d'abord les variables qualitatives et l'identifiant de la station, l'altitude,
# #la longitude et la latitude
# #donc on sélectionne les variables quantitatives que l'on veut garder
# selection = Selection(['pmer', 'tend', 'cod_tend', 'dd', 'ff', 't', 'td', 'u', 'vv', 
# 'ww', 'w1', 'w2', 'n', 'nbas', 'hbas', 'cl', 'cm', 'ch', 'pres', 'niv_bar', 'geop', 'tend24', 
# 'tn12', 'tn24', 'tx12', 'tx24', 'tminsol', 'sw', 'tw', 'raf10', 'rafper', 'per', 'etat_sol', 
# 'ht_neige', 'ssfrai', 'perssfrai', 'rr1', 'rr3', 'rr6', 'rr12', 'rr24', 'phenspe1', 'phenspe2', 
# 'phenspe3', 'phenspe4', 'nnuage1', 'ctype1', 'hnuage1', 'nnuage2', 'ctype2', 'hnuage2', 'nnuage3', 
# 'ctype3', 'hnuage3', 'nnuage4', 'ctype4', 'hnuage4','Region'])
# selection.appliquer(table_meteo)


# aggregation = Agregation('Region')
# aggregation.appliquer(table_meteo)

# #On fait la même chose avec la table électricité
# #On sélectionne les variables qui nous intéressent, ici les variables quantitatives
# selection2 = Selection(['consommation_brute_gaz_grtgaz', 'consommation_brute_electricite_rte',
# 'consommation_brute_totale','consommation_brute_gaz_totale','consommation_brute_gaz_terega','region'])
# selection2.appliquer(table_electricite)

# #On aggrège selon les régions
# aggregation2 = Agregation('region')
# aggregation2.appliquer(table_electricite)


# #On peut maintenant joindre la table d'électricité et météo en fonction de la région

# #On renomme la variable region dans la table d'électricité
# table_electricite.rename_var('Region','region')

# # On effectue la jointure
# jointure2 = Jointure('Region','full_join',table_meteo)
# jointure2.appliquer(table_electricite)


# #On peut, par exemple sélectionner les régions où la consommation brute totale est supérieur à 10000
# def f(x): 
#     return float(x)>=10000

# selectionligne = SelectionLigne('consommation_brute_totale',f)
# selectionligne.appliquer(table_electricite)
# # for i in range(4):
# #     print(table_electricite.body[i])

# #Exportation de la table
# export = ExportCsv("test_main.csv","C:/Users/melis/OneDriveGENES/Documents/1A/projet-info/ptd")
# export.appliquer(table_electricite)

#####################################################
imp_csv2 = ImportCsv("postesSynopAvecRegions.csv","C:/Users/melis/OneDriveGENES/Documents/1A/projet-info/ptd","ISO-8859-1")
table_station_region = imp_csv2.appliquer()

imp_csv = ImportCsv("synop.202201.csv.gz","C:/Users/melis/OneDriveGENES/Documents/1A/projet-info/ptd","ISO-8859-1")
table_meteo = imp_csv.appliquer()
table_meteo.rename_var('ID','numer_sta')

importer_ex = ImportJson("2022-01.json.gz","C:/Users/melis/OneDriveGENES/Documents/1A/projet-info/ptd","ISO-8859-1")
table_electricite = importer_ex.appliquer()

c = Jointure('ID','full_join',table_station_region)

d = Selection(['t','u','ff','tn24','tx24','Region','rr24','date'])


def selection_Bretagne(x):
    return x == 'Bretagne'

e = SelectionLigne('Region',selection_Bretagne)
l = Selection(['t','u','ff','tn24','tx24','rr24','date'])

pipeline_meteo = Pipeline()
liste = [c,d,e,l]
for i in liste:
    pipeline_meteo.ajoute_transformations(i)

pipeline_meteo.run(table_meteo)


g = Selection(['region','date','consommation_brute_electricite_rte','consommation_brute_gaz_totale'])
h = SelectionLigne('region',selection_Bretagne)




i = Selection(['date','consommation_brute_electricite_rte','consommation_brute_gaz_totale','t','u','ff','tn24','tx24','rr24'])
j = Agregation('date')

liste2 = [g,h,i,j]
pipeline_electricite = Pipeline()
for i in liste2:
    pipeline_electricite.ajoute_transformations(i)

pipeline_electricite.run(table_electricite)

for ligne in table_meteo.body:
    ligne[0]=ligne[0][0:8]

k = Agregation('date')
k.appliquer(table_meteo)

for ligne in table_electricite.body:
    ligne[0]=ligne[0].replace("-","")
    if ligne[0]=='20221101':
        ligne[0]='20220111'
    if ligne[0]=='20221201':
        ligne[0]='20220112'
    if ligne[0]=='20221001':
        ligne[0]='20220110'
    if ligne[0]=='20220901':
        ligne[0]='20220109'
    if ligne[0]=='20220801':
        ligne[0]='20220108'
    if ligne[0]=='20220701':
        ligne[0]='20220107'
    if ligne[0]=='20220601':
        ligne[0]='20220106'
    if ligne[0]=='20220501':
        ligne[0]='20220105'
    if ligne[0]=='20220401':
        ligne[0]='20220104'
    if ligne[0]=='20220301':
        ligne[0]='20220103'
    if ligne[0]=='20220201':
        ligne[0]='20220102'
    if ligne[0]=='20220101':
        ligne[0]='20220101'



m = Jointure('date','full_join',table_electricite)
p = CentrageNormalisation(['consommation_brute_electricite_rte','consommation_brute_gaz_totale',])
o = ExportCsv('demo.csv',"C:/Users/melis/OneDriveGENES/Documents/1A/projet-info/ptd")

liste3 = [m,p,o]
pipeline_fin = Pipeline()
for i in liste3:
    pipeline_fin.ajoute_transformations(i)

pipeline_fin.run(table_meteo)


